import scrapy
from scrapy.crawler import CrawlerProcess
from config import Books, db
import config



START_URLS = []


INPUT_FILE_NAME = "second_link_to_books.txt"
INTERVAL_START = 0
INTERVAL_END = 20174

def build_start_urls():
  print "\n\nI AM HERE\n\n"
  global START_URLS
  in_file = open(INPUT_FILE_NAME, "r")


  for i in range(0, INTERVAL_START):
    in_file.readline()

  for i in range(INTERVAL_START, INTERVAL_END):
    link = in_file.readline()
    print link
    if link.find("http")!= 0:
      link = "https://" + link
    START_URLS.append(link)


build_start_urls()



class carturestiScraper(scrapy.Spider):
    name = 'carturestiScraper'
    start_urls = START_URLS
   
    def parse(self, response):
      print "\n\n\n"
      print "*************"
      print "Date"
      print "*************"
      # print response.css('title::text').extract()
      title = response.xpath("//h2[@class='titluProdus']/text()").extract_first()
      productAttr = response.xpath("//div[@class='productAttr']").extract()

      editura = ""
      isbn = ""

      for i in range(0, len(productAttr)):
      	item = productAttr[i]
      	attribute = item[item.find('Label">')+7:item.find('</div>')]
      	if attribute.find('Editura') == 0:
      		editura = attribute[attribute.find('">')+2:attribute.find('</a')]
      	if attribute.find('ISBN') == 0:
      		isbn_slice = str(attribute)
      		isbn =  isbn_slice[isbn_slice.find('ISBN:')+17::]

      price = response.xpath("//span[@itemprop='price']/text()").extract_first()
      author = response.xpath("//div[@class='autorProdus']").extract_first()
      author = author[author.find('href="')+6:]
      author = author[author.find('">')+2:author.find('</')]

      link = response.request.url
      print "current url:", link
      print "*************"

      print "Titlu produs:", title
      print "Autor:", author
      print "Editura:", editura
      print "ISBN:", isbn
      print "Pret produs:", price
      print "*************\n\n\n"

      book = Books(title=title, editura=editura, author=author, isbn=isbn, price=price, link=link)
      db.session.add(book)
      


process = CrawlerProcess({
    'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
})

process.crawl(carturestiScraper)
process.start() # the script will block here until the crawling is finished
db.session.commit()