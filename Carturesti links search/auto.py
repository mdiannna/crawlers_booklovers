import os
from flask.ext.sqlalchemy import SQLAlchemy
from flask import Flask
from flask.ext.migrate import Migrate, MigrateCommand
from flask_script import Manager
import time

app = Flask(__name__)


app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///booklovers.db'
app.config['DEBUG'] = True

db = SQLAlchemy(app)	
db.create_all()
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)


class Books(db.Model):
    __tablename__ = 'books'
    book_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode(100), nullable=False)
    author = db.Column(db.Unicode(100))
    isbn = db.Column(db.Unicode(50))
    bar_code = db.Column(db.Unicode(50))
    editura = db.Column(db.Unicode(100))


for id in range(95000, 96730):
	print "\n\n----------" + str(id) + "-------------"
	book = Books.query.filter(Books.book_id==id)
	if book[0].isbn:
		print book[0].author
		print book[0].title
		print book[0].isbn
		print "\n"
		QUERY = str(book[0].title).replace("UB ", "").replace(" ", "+")
		os.system("python carturesti_db.py " + str(book[0].isbn) + " " + QUERY)