# -*- coding: utf-8 -*-
import scrapy
from twisted.internet import reactor, defer
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
import time
from scrapy import signals,log
from twisted.internet import reactor
from scrapy.crawler import Crawler
from scrapy.settings import Settings


import sys
import time



LINK_CARTE = ''

def search_scrape_function(ISBN, QUERY):
    global LINK_CARTE
    class SearchSpider(scrapy.Spider):
        name = "example"
        
        start_urls = (
            'https://www.bing.com/search?q=site%3Acarturesti.ro+' + QUERY,
        )

        def start_requests(self):
            for url in self.start_urls:
                yield scrapy.Request(url, self.parse, meta={
                    'splash': {
                        'endpoint': 'render.html',
                        'args': {'wait': 0.5}
                    }
                })

        def parse(self, response):
            global LINK_CARTE
            print "\n\n\n"
           

            LINK = str(response.xpath("//div[@class='b_attribution']").extract_first())
            LINK = LINK[LINK.find("<cite>")+6:LINK.find("</cite>")].replace("<strong>", "").replace("</strong>", "")
            print "LINK:", LINK

            LINK2 = response.css("h2").extract_first()
            print "RAW LINK2:"
            LINK2 = LINK2[LINK2.find("=")+2:LINK2.find('" h')]
            print "LINK2:", LINK2
        

            LINK_CARTE = LINK2



    configure_logging()
    runner = CrawlerRunner()

    @defer.inlineCallbacks
    def crawl():
        yield runner.crawl(SearchSpider)
        print "Link carte:", LINK_CARTE
        reactor.stop()

    crawl()
    reactor.run()

    return LINK_CARTE


ISBN = sys.argv[1]
QUERY = sys.argv[2]


link = search_scrape_function(ISBN, QUERY)
print "link carte: ", link


f = open("link_to_books3.txt", "a")
if link and link != '">Try one of these related searches</h2':
    f.write(link +"\n")
f.close()