from scrapy.crawler import CrawlerProcess
from outputdatabase import db, Books


START_URLS = []


INPUT_FILE_NAME = "isbn_booklovers.txt"
INTERVAL_START = 0
INTERVAL_END = 44131

def build_start_urls():
  print "\n\nBUILDING START URLS\n\n"
  global START_URLS
  in_file = open(INPUT_FILE_NAME, "r")


  for i in range(0, INTERVAL_START):
    in_file.readline()

  for i in range(INTERVAL_START, INTERVAL_END):
    link = in_file.readline().replace("\n", "")
    START_URLS.append('http://www.librarie.net/cautare-rezultate.php?t=' + link)


build_start_urls()



class carturestiScraper(scrapy.Spider):
    name = 'carturestiScraper'
    
    start_urls = START_URLS
    cnt = 0
   
    def parse(self, response):
		self.cnt = self.cnt+1
		print "-------", self.cnt, "-------"
		item = response.css('b::text').extract()
		titlu = item[3]

		item = response.css('img').extract()
		img = item[3]
		img = img[img.find("=")+2:img.find('" width=')]

		pret = response.xpath("//span[@class='red']/text()").extract_first()


		link = response.request.url

		if pret:
			print "\n\n\n"
			print "*************"
			print "Date"
			print "*************"
			print "titlu:", titlu
			print "img:", img
			print "pret:", pret
			print "*************"
			print "link:", link
			print "*************\n"
			isbn = link[link.find('t=')+2:]
			print "isbn:", isbn


			book = Books(title=titlu, img=img, price=pret, link=link, isbn=isbn)
			db.session.add(book)



process = CrawlerProcess({
    'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
})

process.crawl(carturestiScraper)
process.start() # the script will block here until the crawling is finished
db.session.commit()

