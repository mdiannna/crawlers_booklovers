import os
from flask.ext.sqlalchemy import SQLAlchemy
from flask import Flask
from flask.ext.migrate import Migrate, MigrateCommand
from flask_script import Manager


app = Flask(__name__)


app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///librarienet.db'
app.config['DEBUG'] = True

db = SQLAlchemy(app)
db.create_all()
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)


class Books(db.Model):
    __tablename__ = 'books'
    book_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode(100), nullable=False)
    # author = db.Column(db.Unicode(100))
    isbn = db.Column(db.Unicode(50))
    link = db.Column(db.Unicode(100))
    # editura = db.Column(db.Unicode(100))
    price = db.Column(db.Unicode(20))
    img = db.Column(db.Unicode(100))


	
# manager.run()